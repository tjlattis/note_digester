#! /usr/local/bin/python3
"""a lighter weight alternative to the JMLNotes project which uses csv for output
   and handles some errors and automated correction with vim"""

import argparse
import time
import csv
import re
import os
from subprocess import call

__author__ = "Anthony Lattis"
__email__ = "tjlattis@gmail.com"

# mode options
OUTPUT_NAME = "digested_notes"
CORRECTION_MODE = False

# compile regexes
UN_DATE_REGEX = re.compile(r'[1-2]\d\d\d\.[0-1]\d\.[0-3]\d')
DATE_REGEX = re.compile(r'(\d+)\s(\w+)\s(\d\d\d\d)(.*)')
EDITOR = os.environ.get('EDITOR', 'vim')

# look up table for month patterns
MONTH_LOOKUP = {'january': '01', 'jan': '01', 'february': '02', 'feb': '02',
                'march': '03', 'mar': '03', 'april': '04', 'apr': '04',
                'may': '05', 'june': '06', 'jun': '06', 'july': '07',
                'jul': '07', 'august': '08', 'aug': '08', 'september': '09',
                'sep': '09', 'sept': '09', 'october': '10', 'oct': '10',
                'november': '11', 'nov': '11', 'december': '12', 'dec': '12'}

def init_csv(target):
    """initialize the output file"""

    csv_name = ""

    #test if directory
    if os.path.isdir(target):
        csv_name = os.path.join(target, OUTPUT_NAME)
    else:
        csv_name = OUTPUT_NAME

    csv_file = open(csv_name, 'w')
    csv_file.close()
    return csv_name

def correct(src_name, string):
    """open editor and regex search for prvoided string"""

    esc_chars = ('/', '[', ']')
    if any(char in string for char in esc_chars):
        print("string {" + string + "} contains characters likely to disrupt " +
              "automatic correction, consider correting manually")
    else:
        call([EDITOR, '+/\\m' + string.strip(), src_name])

def print_error(desc, string, src_name):
    """formulate and print and error message"""

    print(desc +' {' + string + '}  in: ' + src_name)

def parse_date(string, src_name):
    """reformats dates to yyyy.mm.dd and attempt to """

    #check for correct date format
    if UN_DATE_REGEX.match(string):
        return string

    # check for an empty string
    if string.strip() == '':
        return 'No Date'

    # search for a parsable date
    res = DATE_REGEX.search(string)

    # if none found try to correct manually
    if res is None:
        if CORRECTION_MODE:
            correct(src_name, string)
            return string

    # parse date, assemble and return
    field = ""

    try:
        month_string = res.group(2)
        month_string = month_string.lower()

        # look up month and check for error
        month_digit = MONTH_LOOKUP.get(month_string)
        if month_digit is None:
            if CORRECTION_MODE:
                correct(src_name, string)
            else:
                print_error('month group not in lookup table', month_string, src_name)
            return string

        # assemble date string
        date = str(res.group(3)) + '.' + month_digit + '.' + str(res.group(1))
        text = str(res.group(4))
        field = date + text

    except AttributeError:
        print_error('unparseable date', string, src_name)

    return field

def write_item(location, item, src_name, csv_name):
    """write item data to csv"""

    # set location values
    repository = location[0]
    series = location[1]
    box = location[2]
    container = location[3]

    # zero values
    persons = ""
    date = ""
    content_list = []

    # loop over item
    for line in item:

        # parse persons line
        if line.startswith('PP'):

            # check for long lines indicationg an error
            if len(line) > 80:
                if CORRECTION_MODE:
                    correct(src_name, line[:10])
                else:
                    print_error('long persons line', line[:30] + '...', src_name)
            persons = line[3:]

        # parse date line
        elif line.startswith('DD'):

            # check for some other common date paterns
            if " n.d." in line:
                date = line[3:]
            elif "various" in line:
                date = line[3:]
            else:
                date = parse_date(line[3:], src_name)

        # otherwise append as note content
        else:
            content_list.append(line)

    # write item to csv
    content = " ".join(str(i) for i in content_list)
    csv_file = open(csv_name, 'a')
    line_writer = csv.writer(csv_file)
    line_writer.writerow([repository, series, box, container, persons, date, content,
                          src_name])

def parse(target, csv_name):
    """loop over file lines and perform parse"""

    # init variables
    item = []
    box = ""
    container = ""
    repository = ""
    series = ""

    # are we currntly reading an item or a note?
    is_item = False
    is_note = False

    # loop over file
    with open(target) as filestream:
        for line in filestream:

            # skip over initial note box
            if '"""' in line:

                if is_note is False:
                    is_note = True
                    continue

                if is_note:
                    is_note = False
                    continue

            if is_note:
                continue

            # check if already in an item
            if is_item:
                if line.startswith(('II', 'Box', 'FF')):
                    location = (repository, series, box, container)
                    write_item(location, item, target, csv_name)
                    location = []
                    item = []
                    is_item = False
                else:
                    item.append(line.strip())
                    continue

            # check for repository
            if line.startswith('RR'):
                repository = line.strip()[3:]
                continue

            # check for series/accession
            if line.startswith('AA'):
                series = line.strip()[3:]
                continue

            # check for box
            if line.startswith('Box'):
                box = line.strip()
                continue

            # check for folder
            if line.startswith('FF'):
                container = "Folder " + line.strip()[3:]
                continue

            # check for quaderno
            if line.startswith('QQ'):
                container = "Notebook " + line.strip()[3:]
                continue

            # begin item cycle
            if line.startswith('II'):
                is_item = True
                continue

        location = (repository, series, box, container)
        write_item(location, item, target, csv_name)

def main():
    """this is main"""

    start_time = time.time()

    # parse argumetns
    parser = argparse.ArgumentParser()
    parser.add_argument("target", action="store", nargs='+', help="specify the target file")
    args = parser.parse_args()

    # loop over provided files
    for arg in args.target:

        csv_name = init_csv(arg)

        if os.path.isdir(arg):
            for input_file in os.listdir(arg):
                print('parse %s' % os.path.join(arg, input_file))
                parse(os.path.join(arg, input_file), csv_name)
        else:
            for input_file in args.target:
                parse(input_file, csv_name)

    print('runtime: %s' % (time.time() - start_time))

main()
